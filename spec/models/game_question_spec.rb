require 'rails_helper'

RSpec.describe GameQuestion, type: :model do

  let(:game_question) { FactoryBot.create(:game_question, a: 4, b: 1, c: 3, d: 2) }

  context 'game status' do
    it 'correct .variants' do
      expect(game_question.variants).to eq ({
                                               'a' => game_question.question.answer4,
                                               'b' => game_question.question.answer1,
                                               'c' => game_question.question.answer3,
                                               'd' => game_question.question.answer2
                                             })
    end

    it 'correct .answer_correct?' do
      expect(game_question.answer_correct?('b')).to be_truthy
    end

    it 'correct .level && .text to delegations(delegate)' do
      expect(game_question.level).to eq(game_question.question.level)
      expect(game_question.text).to eq(game_question.question.text)
    end
  end
end
